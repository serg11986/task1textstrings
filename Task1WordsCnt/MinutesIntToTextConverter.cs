﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Task1WordsCnt
{
    /// <summary>
    /// Class for converting double value and string text to string
    /// (if parameter string is empty, it adds integer prefix)
    /// </summary>
    public class MinutesDoubleAndStringToMinutesStringConverter : IMultiValueConverter
    {

        /// <summary>
        /// converts double value and string text to string
        /// (if parameter string is empty, adds integer prefix)
        /// </summary>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue)
                return "";
            int i = (int)(double)values[0];
            string s = (string)values[1];
            string prefix = string.IsNullOrWhiteSpace(s) ? $"{i} " : "";
            int rem100 = i % 100;
            if ((rem100 >= 5 && rem100 <= 20) || rem100 % 10 == 0)
                return $"{prefix}минут";
            int rem10 = rem100 % 10;
            if (rem10 == 1)
                return $"{prefix}минута";
            if (rem10 >= 2 && rem10 <= 4)
                return $"{prefix}минуты";
            return $"{prefix}минут";
        }

        /// <summary>
        /// This method is not supported: class is intended for one-way convertion
        /// </summary>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
