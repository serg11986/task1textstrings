﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace Task1WordsCnt
{
    /// <summary>
    /// Class for in-memory storing of T-type data
    /// retrieved by async function (passed in constructor)
    /// Time of storing data brfore update 
    /// can be configured by UpdateTimeMs property
    /// </summary>
    /// <typeparam name="T">Type of stored data</typeparam>
    public class ArrayCacheAsync<T> : INotifyPropertyChanged
    {
        private (T Data, DateTime Updated, bool IsCorrupted)?[] cache;
        private readonly Func<int, Task<T>> getFuncAsync;
        private readonly Func<T, bool> checkCorruptedFunc;
        private bool isEmpty = true;

        /// <summary>
        /// Indicates that property has been chanhed (see property Name in EventArgs)
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///  Number of minutes during which information can be cached without updating
        /// </summary>
        public int UpdateTimeInMinutes { get; set; }

        /// <summary>
        /// Defines if cache has no entries (is empty)
        /// </summary>
        public bool IsEmpty
        {
            get => isEmpty;
            set
            {
                isEmpty = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsEmpty)));
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="capacity">Capacity of cache</param>
        /// <param name="getFunc">Fuction that will be used to get a data by index</param>
        public ArrayCacheAsync(int capacity, Func<int, Task<T>> getFuncAsync, Func<T, bool> checkCorruptedFunc, int updateTimeInMinutes)
        {
            this.getFuncAsync = getFuncAsync;
            this.checkCorruptedFunc = checkCorruptedFunc;
            cache = new (T, DateTime, bool)?[capacity];
            UpdateTimeInMinutes = updateTimeInMinutes;
        }

        /// <summary>
        /// Returns data by index (works asynchronously)
        /// If data by this index hasn't been retrieved yet or been retrieved with errors (IsCorrupted property) or time elapsed from last
        /// update of data is more or equal UpdateTimeMs property, it retrieves and returns
        /// data using getFunc function, otherwise it returns pre-stored data by this index
        /// </summary>
        /// <param name="i">Index</param>
        /// <returns></returns>
        public Task<T> this[int i] => GetDataAsync(i);


        /// <summary>
        /// Returns data by index (works asynchronously)
        /// If data by this index hasn't been retrieved yet or been retrieved with errors (IsCorrupted property) or time elapsed from last
        /// update of data is more or equal UpdateTimeMs property, it retrieves and returns
        /// data using getFunc function, otherwise it returns pre-stored data by this index
        /// </summary>
        /// <param name="i">Index</param>
        private async Task<T> GetDataAsync(int i)
        {
            if (i < 0 || i > cache.Length)
                throw new ArgumentException("Индекс вне границ диапазона");
            if (cache[i] == null || cache[i].Value.IsCorrupted || (DateTime.Now - cache[i].Value.Updated).TotalMinutes >= UpdateTimeInMinutes)
            {
                T data = await getFuncAsync(i);
                bool isCorrupted = checkCorruptedFunc(data);
                if (!isCorrupted)
                    IsEmpty = false;
                cache[i] = (data, DateTime.Now, isCorrupted);
            }
            return cache[i].Value.Data;
        }

        /// <summary>
        /// Cleares cache
        /// </summary>
        public void Clear()
        {
            for (int i = 0; i < cache.Length; i++)
                cache[i] = null;
            IsEmpty = true;
        }
    }
}
