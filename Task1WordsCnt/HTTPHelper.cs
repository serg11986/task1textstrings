﻿using System.Linq;
using System.Net.Http;
using System.Text;

namespace Task1WordsCnt
{
    /// <summary>
    /// Helper static class, providing some additional functionality for working with HTTP
    /// </summary>
    public static class HTTPHelper
    {
        /// <summary>
        /// Parses HTTP status code from HttpRequestException ex
        /// </summary>
        public static int GetStatusCode(HttpRequestException ex)
        {
            var potentialCodeStarts = ex.Message.Split(':').Select(s1 => s1.Trim()).Where(s2 => char.IsDigit(s2[0])).ToArray();
            if (potentialCodeStarts.Length != 1)
                return -1;
            string s = potentialCodeStarts[0];
            var sb = new StringBuilder();
            int i = 0;
            while (char.IsDigit(s[i]))
            {
                sb.Append(s[i]);
                i++;
            }
            return int.Parse(sb.ToString());
        }
    }
}
