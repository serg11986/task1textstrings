﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace Task1WordsCnt
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Fields

        /// <summary>
        /// Controller object which encapsulates all data retrieving logic
        /// </summary>
        private Controller controller;

        /// <summary>
        /// Indicates if RichTextBox contains bad (error) id strings
        /// </summary>
        private bool hasBadIds;

        /// <summary>
        /// Delimeters (characters that is used for splitting) for id strings
        /// </summary>
        private char[] delimeters = new[] { ' ', ',', ';' };

        /// <summary>
        /// Stores default decorations of text in RichTextBox
        /// (without any highlighting)
        /// </summary>
        private TextDecorationCollection defaultDecorationsRtb;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            controller = new Controller();
            DataContext = controller;
            defaultDecorationsRtb = new TextRange(rtbIds.Document.ContentStart, rtbIds.Document.ContentEnd).GetPropertyValue(Inline.TextDecorationsProperty) as TextDecorationCollection;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Parses RichTextBox text and returns unique correct ids in its text
        /// </summary>
        private IEnumerable<int> GetInputIds() =>
                 GetInputIdsText()
                .Split(delimeters)
                .Distinct()
                .Where(IsCorrectIdString)
                .Select(s => int.Parse(s));

        /// <summary>
        /// Returns RichTextBox text
        /// </summary>
        private string GetInputIdsText() => new TextRange(rtbIds.Document.ContentStart, rtbIds.Document.ContentEnd).Text;

        /// <summary>
        /// Method that starts calculating proccess and also
        /// indicates of its end (if checkBox "notify fin calculation is checked)
        /// and of some errors in RichTextBox text
        /// (if they exist and checkBox "show errors" is checked)
        /// </summary>
        private async Task Calculate()
        {
            if (hasBadIds && cbShowWarnings.IsChecked.HasValue && cbShowWarnings.IsChecked.Value)
                MessageBox.Show(
                    "Поле ввода содержит некорректные идентификаторы, они выделены красным и подчеркнуты.\r\n" +
                    $"Корректным идентификатором является число от 1 до {Controller.TextsCount}\r\n" +
                    "Некорректные идентификаторы при подсчете будут проигнорированы"
                    );
            List<int> inputIds = GetInputIds().ToList();
            if (inputIds.Count == 0 && cbShowWarnings.IsChecked.HasValue && cbShowWarnings.IsChecked.Value)
                MessageBox.Show(
                    $"Вы не ввели ни одного {(hasBadIds ? "корректного " : "")}идентификатора." +
                    $"{(controller.CurrentTextsStats.Count > 0 ? " Таблица будет очищена" : "")}");
            Cursor = Cursors.Wait;
            await controller.UpdateCurrentTextsStatsAsync(GetInputIds());
            Cursor = Cursors.Arrow;
            if (inputIds.Count > 0 && cbNotifyFinCalc.IsChecked.HasValue && cbNotifyFinCalc.IsChecked.Value)
                MessageBox.Show("Подсчет завершен");
        }


        /// <summary>
        /// Returns text range from RichTextBox by index (in text of RichTextBox) 
        /// and length of text in this text range
        /// </summary>
        /// <returns></returns>
        private TextRange SelectRangeFromRtb(int index, int length)
        {
            TextRange textRange = new TextRange(rtbIds.Document.ContentStart, rtbIds.Document.ContentEnd);
            TextPointer start = textRange.Start.GetPositionAtOffset(index, LogicalDirection.Forward);
            TextPointer end = textRange.Start.GetPositionAtOffset(index + length, LogicalDirection.Backward);
            return new TextRange(start, end);
        }


        /// <summary>
        /// Defines if string can be parsed as correct id
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private bool IsCorrectIdString(string s)
        {
            if (!int.TryParse(s, out int i))
                return false;
            return i > 0 && i <= Controller.TextsCount;

        }

        /// <summary>
        /// Returns all text ranges from RichTextBox that contain incorrect id strings
        /// </summary>
        private List<TextRange> GetIncorrectRanges()
        {
            string txt = GetInputIdsText();
            var badRanges = new List<TextRange>();
            var badIdsStrings = txt.Split(delimeters).
                Where(s => !string.IsNullOrWhiteSpace(s)).
                Select(s => s.Trim()).
                Where(s => !IsCorrectIdString(s)).ToList();

            foreach (string badIdString in badIdsStrings)
            {
                var badIndexes = txt.AllIndexesOf(badIdString).ToList();
                foreach (int i in badIndexes)
                {
                    badRanges.Add(SelectRangeFromRtb(i, badIdString.Length));
                }
            }

            return badRanges;
        }

        /// <summary>
        /// Highlights text in range
        /// </summary>
        private void HighlightTextInRange(TextRange range)
        {
            TextDecorationCollection tdc_clone = new TextDecorationCollection(defaultDecorationsRtb);
            tdc_clone.Add(TextDecorations.Underline);
            range.ApplyPropertyValue(Inline.TextDecorationsProperty, tdc_clone);
            range.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.Red);
        }

        /// <summary>
        /// Clears RichTextBox text highlighting
        /// </summary>
        private void ClearRtbErrorsHighlighting()
        {
            if (defaultDecorationsRtb == null)
                return;
            var range = new TextRange(rtbIds.Document.ContentStart, rtbIds.Document.ContentEnd);
            range.ApplyPropertyValue(Inline.TextDecorationsProperty, defaultDecorationsRtb);
            range.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.Black);
        }

        /// <summary>
        /// Highlites bad index strings in RichTextBox text
        /// </summary>
        private void HighlightRtbErrors()
        {
            if (defaultDecorationsRtb == null)
                return;
            ClearRtbErrorsHighlighting();
            List<TextRange> rangesWithErrors = GetIncorrectRanges();
            hasBadIds = rangesWithErrors.Count > 0;
            foreach (var badRange in rangesWithErrors)
                HighlightTextInRange(badRange);
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Handling of Button click to start calculation 
        /// </summary>
        private async void BtnCalcClick(object sender, RoutedEventArgs e)
        {
            await Calculate();
        }

        /// <summary>
        /// Handles PreviewTextInput for TextBox "update time"
        /// This handler keeps TextBox "update time" having only
        /// empty string or integer between 1 and 30 
        /// (allowed minutes for cache update time in minutes)
        /// as its text
        /// </summary>
        private void TbUpdTime_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int value;

            if (!int.TryParse(e.Text, out value))
            {
                e.Handled = true;
                return;
            }

            var tb = sender as TextBox;
            string textBeforeSelection = tb.Text.Substring(0, tb.SelectionStart);
            string textAfterSelection = tb.Text.Substring(tb.SelectionStart + tb.SelectionLength);
            string newText = textBeforeSelection + e.Text + textAfterSelection;

            bool isInt = int.TryParse(newText, out value);
            e.Handled = !isInt || value < 1 || value > 30;
        }

        /// <summary>
        /// Clear cache button Click handler
        /// </summary>
        private void BtnClearCacheClick(object sender, RoutedEventArgs e)
        {
            controller.ClearCache();
        }


        /// <summary>
        /// Key down handler for indexes input RichTextBox
        /// </summary>
        private void rtbIds_KeyDown(object sender, KeyEventArgs e)
        {
            //Preventing typing in new line (keep RichTextBox containing
            //only one line - which makes highlighting errors easier
            //and makes typing indexes more clear for user)
            if (GetInputIdsText().EndsWith("\r\n\r\n"))
                e.Handled = true;
        }


        /// <summary>
        /// Key up handler for indexes input RichTextBox
        /// </summary>
        private async void rtbIds_KeyUp(object sender, KeyEventArgs e)
        {
            //On enter press we run calculation process and also
            //preventing more than one line addind (keeping RichTextBox containing
            //makes highlighting errors easier
            //and makes typing indexes more clear for user)
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                string trimmedText = GetInputIdsText().Trim();
                new TextRange(rtbIds.Document.ContentStart, rtbIds.Document.ContentEnd).Text = trimmedText;

                await Calculate();
            }

            HighlightRtbErrors();
        }


        /// <summary>
        /// Mouse up handler for indexes input RichTextBox
        /// </summary>
        private void rtbIds_MouseUp(object sender, MouseButtonEventArgs e)
        {
            HighlightRtbErrors();
        }

        #endregion
    }
}
