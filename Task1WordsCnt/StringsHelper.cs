﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task1WordsCnt
{
    /// <summary>
    /// Helper static class, providing some additional functionality for working with strings
    /// </summary>
    public static class StringsHelper
    {
        /// <summary>
        /// Set of most popular vowels in European languages (it may be incomplete) in a lower case
        /// </summary>
        private static HashSet<char> lowerVowels = new HashSet<char>("éàèùòâêîôûëïüÿaeiouyöæáíóúýęœøäåаоиеёэыуюяαεηιουω");

        /// <summary>
        /// Delimeters for words in phrase
        /// </summary>
        private static char[] delimeters = " ,;:?%!—()[]{}<>\"".ToCharArray();

        /// <summary>
        /// Checks if the letter is a vowel in a lower case
        /// </summary>
        /// <param name="c">Letter</param>
        /// <returns></returns>
        public static bool IsLowerVowel(char c) => lowerVowels.Contains(c);

        /// <summary>
        /// Extension method for strings, counts number of a vowel chars in the string
        /// </summary>
        /// <param name="phrase">String (prhase) to count the number of vowels</param>
        /// <returns></returns>
        public static int VowelsCount(this string phrase) => phrase.ToLower().Count(IsLowerVowel);

        /// <summary>
        /// Extension method for strings, counts number of a words in the phrase 
        /// (assuming that words in phrase are separated by a space symbols)
        /// </summary>
        /// <param name="phrase">String (prhase) to count the number of words</param>
        public static int WordsCount(this string phrase) =>
            phrase.
            Split(delimeters.ToArray()).
            Count(s => s.Any(char.IsLetterOrDigit));

        /// <summary>
        /// Returns start indexes of all string "value" occurrences in string "str"
        /// https://stackoverflow.com/questions/2641326/finding-all-positions-of-substring-in-a-larger-string-in-c-sharp
        /// </summary>
        public static IEnumerable<int> AllIndexesOf(this string str, string value)
        {
            if (string.IsNullOrEmpty(value))
                throw new ArgumentException("the string to find may not be empty", "value");
            for (int index = 0; ; index += value.Length)
            {
                index = str.IndexOf(value, index);
                if (index == -1)
                    break;
                yield return index;
            }
        }
    }
}
