﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Configuration;
using System.ComponentModel;
using static Task1WordsCnt.HTTPHelper;

namespace Task1WordsCnt
{
    /// <summary>
    /// Class that encapsulates logic of data (strings) retrieving and storing
    /// </summary>
    public class Controller : INotifyPropertyChanged
    {

        #region Constants

        /// <summary>
        /// Count of texts provided by api
        /// </summary>
        public const int TextsCount = 20;

        #endregion

        #region Fields

        /// <summary>
        /// Defines if request results will be stored in cache or not
        /// </summary>
        private bool useCache = true;

        /// <summary>
        /// Number of minutes during which information can be cached without updating
        /// </summary>
        private int cacheUpdateTimeInMinutes;

        /// <summary>
        /// Defines if cache has entries (is not empty)
        /// </summary>
        private bool cacheHasEntries = false;

        /// <summary>
        /// Defines if controller is free now (doesn't downloading and analysing information)
        /// </summary>
        private bool isFree = true;

        /// <summary>
        /// Fixed part of URI (adress of server)
        /// </summary>
        private string urlFixedPart;

        /// <summary>
        /// Api header name
        /// </summary>
        private string apiKeyHeaderName;

        /// <summary>
        /// Api key 
        /// </summary>
        private string apiKey;

        /// <summary> 
        /// Cache object which stores information previously retrieved from server
        /// </summary>
        private ArrayCacheAsync<TextStats> cache;

        /// <summary>
        /// Request manager object. All HTTP requests to server are executing with its help
        /// </summary>
        private HTTPRequestManager requestManager;

        #endregion

        #region Properties


        /// <summary>
        /// Text Stats retrieved and calculated during last request
        /// </summary>
        public ObservableCollection<TextStats> CurrentTextsStats { get; set; } = new ObservableCollection<TextStats>();

        /// <summary>
        /// Defines if request results will be stored in cache or not
        /// </summary>
        public bool UseCache
        {
            get => useCache;
            set
            {
                useCache = value;
                NotifyPropertyChanged(nameof(UseCache));
            }
        }

        /// <summary>
        /// Number of minutes during which information can be cached without updating
        /// </summary>
        public int CacheUpdateTimeInMinutes
        {
            get => cacheUpdateTimeInMinutes;
            set
            {
                cacheUpdateTimeInMinutes = cache.UpdateTimeInMinutes = value;
                NotifyPropertyChanged(nameof(CacheUpdateTimeInMinutes));
            }
        }

        /// <summary>
        /// Defines if cache has entries (is not empty)
        /// </summary>
        public bool CacheHasEntries
        {
            get => cacheHasEntries;
            set
            {
                cacheHasEntries = value;
                NotifyPropertyChanged(nameof(CacheHasEntries));
            }
        }

        /// <summary>
        /// Defines if controller is free now (doesn't downloading and analysing information)
        /// </summary>
        public bool IsFree
        {
            get => isFree;
            set
            {
                isFree = value;
                NotifyPropertyChanged(nameof(IsFree));
            }
        }

        #endregion

        #region Constructors

        public Controller(int updateTimeMinutes = 1)
        {
            ReadConnectionStrings();
            requestManager = new HTTPRequestManager(new (string, string)[] { (apiKeyHeaderName, apiKey) });
            cache = new ArrayCacheAsync<TextStats>(TextsCount + 1,
                DownloadTextAndCalcStatsAsync, ts => ts.VowelsCount == null, updateTimeMinutes);
            CacheUpdateTimeInMinutes = updateTimeMinutes;
            cache.PropertyChanged += Cache_PropertyChanged;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Reads connection strings from App.config file
        /// </summary>
        private void ReadConnectionStrings()
        {
            urlFixedPart = ConfigurationManager.ConnectionStrings["UrlFixedPart"].ConnectionString;
            apiKeyHeaderName = ConfigurationManager.ConnectionStrings["ApiKeyHeader"].ConnectionString;
            apiKey = ConfigurationManager.ConnectionStrings["ApiKey"].ConnectionString;
        }

        /// <summary>
        /// Raises PropertyChangedEvent with propertyName in its EventArgs
        /// </summary>
        private void NotifyPropertyChanged(string propertyName) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));


        /// <summary>
        /// Handling cache PropertyChanged event. Updates CacheHasEntries property
        /// when cache IsEmpty property has been updated
        /// </summary>
        private void Cache_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(cache.IsEmpty))
                CacheHasEntries = !cache.IsEmpty;
        }

        /// <summary>
        /// Downloads text from server by index (works asynchronously)
        /// </summary>
        private async Task<string> DownloadTextAsync(int i)
        {
            var dynamicObject = await requestManager.DownloadDynamicObjectAsync($"{urlFixedPart}/{i}");
            return dynamicObject.text;
        }

        /// <summary>
        /// Downloads text from server by index and calculates required statistics 
        /// If error occures during text download attempt returns text without statistics
        /// that contains error description
        /// (works asynchronously)
        /// </summary>
        private async Task<TextStats> DownloadTextAndCalcStatsAsync(int i)
        {
            string text;
            try
            {
                text = await DownloadTextAsync(i);
            }
            catch (HttpRequestException ex)
            {
                string txt = $"При загрузке строки с номером {i} возникла ошибка. Повторите попытку позже.\r\n\r\nТекст ошибки:\r\n{ex.Message}";
                int statusCode = GetStatusCode(ex);
                if (statusCode > 0)
                    txt += $"\r\n\r\nСведения об ошибке см. по адресу:\r\nhttps://ru.wikipedia.org/wiki/Список_кодов_состояния_HTTP#{statusCode}";
                return new TextStats { Text = txt };
            }
            catch (Exception ex)
            {
                return new TextStats
                {
                    Text = $"При загрузке строки с номером {i} возникла ошибка. Повторите попытку позже. Текст ошибки:\r\n\r\n{ex.Message}"
                };
            }
            return new TextStats { Text = text, VowelsCount = text.VowelsCount(), WordsCount = text.WordsCount() };
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Updates current TextsStats (from cache or by making request 
        /// depending of whether a cache is been used [UseCache property]
        /// and, if used, have fresh entries by indexes in "indexes" parameter)
        /// (works asynchronously)
        /// </summary>
        public async Task UpdateCurrentTextsStatsAsync(IEnumerable<int> indexes)
        {
            if (!IsFree)
                return;
            IsFree = false;
            CurrentTextsStats.Clear();
            indexes = indexes.Distinct().Where(i => i > 0 && i <= TextsCount);
            if (!indexes.Any())
            {
                IsFree = true;
                return;
            }
            foreach (int i in indexes)
            {
                var txtWithStat = UseCache ? await cache[i] : await DownloadTextAndCalcStatsAsync(i);
                if (txtWithStat != null)
                {
                    CurrentTextsStats.Add(txtWithStat);
                }
            }
            IsFree = true;
        }

        /// <summary>
        /// Clears cache
        /// </summary>
        public void ClearCache() => cache.Clear();

        #endregion

        #region Events

        /// <summary>
        /// Indicates that property has been chanhed (see property Name in EventArgs)
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion























        



        

        
        

        

        




    }
}
