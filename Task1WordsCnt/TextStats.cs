﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1WordsCnt
{
    /// <summary>
    /// Class storing text and some statistics about it
    /// </summary>
    public class TextStats
    {
        /// <summary>
        /// Text
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Count of vowels in text
        /// </summary>
        public int? VowelsCount { get; set; }

        /// <summary>
        /// Count of words in text
        /// </summary>
        public int? WordsCount { get; set; }

        public override string ToString()
        {
            return $"{Text} (гласных букв: {VowelsCount}, всего слов: {WordsCount})";
        }
    }
}
