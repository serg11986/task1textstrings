﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Task1WordsCnt
{
    /// <summary>
    /// Class that encapsulates HTTP requests execution logic
    /// </summary>
    public class HTTPRequestManager
    {
        private HttpClient client = new HttpClient();

        /// <summary>
        /// Constructor. Allows adding headers to HttpClient
        /// </summary>
        public HTTPRequestManager(IEnumerable<(string headerName, string haderValue)> headers)
        {
            foreach(var (headerName, headerValue) in headers)
            {
                client.DefaultRequestHeaders.Add(headerName, headerValue);
            }
            
        }

        /// <summary>
        /// Makes request and returns its result as dynamic object (works asynchronously)
        /// </summary>
        public async Task<dynamic> DownloadDynamicObjectAsync(string url)
        {
            Task<string> getStringTask = client.GetStringAsync(url);
 
            string json = await getStringTask;
 
            return JsonConvert.DeserializeObject(json);
        }
    }
}
